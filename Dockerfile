FROM registry.gitlab.com/thallian/docker-confd-env:master

RUN addgroup -g 2222 ldap
RUN adduser -h /var/lib/ldap -u 2222 -D -G ldap ldap

ENV BACKUP_DIR /var/lib/ldap/backup
ENV BACKUP_KEEP_DAYS 7
ENV BACKUP_KEEP_WEEKS 4
ENV BACKUP_KEEP_MONTHS 6
ENV CRON_SCHEDULE */15  *  *  *  *

RUN apk add --no-cache openldap-clients

RUN mkdir -p $BACKUP_DIR
RUN chown -R ldap:ldap /var/lib/ldap

ADD /rootfs /

VOLUME $BACKUP_DIR
