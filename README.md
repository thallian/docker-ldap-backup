Periodic backups of ldap databases.

# Volumes
- `/var/lib/ldap/backup`

# Environment Variables
## CRON_SCHEDULE
- default: */15  *  *  *  *

When to run the backup script, in crontab format.

## LDAP_HOST

The ldap host to connect to.

## LDAP_USER

User for the ldap connection.

## LDAP_PASSWORD

Password for the ldap connection.

## LDAP_BASE_DN

Base DN of the backup.

## BACKUP_KEEP_DAYS
- default: 7

How many daily backups to keep.

## BACKUP_KEEP_WEEKS
- default: 4

How many weekly backups to keep.

## BACKUP_KEEP_MONTHS
- default: 6

How many monthly backups to keep.

# Capabilities
- CHOWN
- DAC_OVERRIDE
- FOWNER
- NET_BIND_SERVICE
- SETGID
- SETUID
